<?php
	require_once "Mail.php"; // Pear Mail Library
	require_once 'dbConnect.php'; // Connect to the database
	
	ob_start();
	session_start();	
	
	if (!$dbconn) {
		echo '{"An error occurred.\n"}';
		exit;
	}

	// data sent from form 
	$myusername=$_POST['form-user-name']; 
	$mydisplayname=$_POST['form-display-name']; 
	$myemail=$_POST['form-email']; 
	$temp=$_POST['form-pass-word']; 
	$mypassword = md5($temp);
	
	// code for the activation
	$code = md5(uniqid(rand()));
	
	// check which exercise the user is registering for
	if ($_SESSION['link'] == 'one') {
		$workspace = 'riskgis'; 
		$id = base64_encode(1);
	}
	elseif ($_SESSION['link'] == 'two') {
		$workspace = 'riskgis_two';
		$id = base64_encode(2);
	}
	elseif ($_SESSION['link'] == 'three') {
		$workspace = 'riskgis_three'; 
		$id = base64_encode(3);
	}
	
	// insert a new user if not already registered with an existing username
	$query="SELECT * FROM $workspace.users WHERE user_name='$myusername';";
	$result = pg_query($dbconn,$query); // query the database
	$count = pg_num_rows($result); // pg_num_row is counting table row
	
	if($count>0){
		$_SESSION['message']= 'This username ('.$myusername.') already exists. Please select a new username and try again!';	
	}
	else {
		$query = "INSERT INTO ".$workspace.".users VALUES (DEFAULT, '$myusername', '$mydisplayname', '$myemail', 0, 1, '$code','$mypassword');";
		$result= pg_query($dbconn,$query);	
		
		// data to send activation link
		$from = 'riskgis.igar.unil@gmail.com';
		$to = $myemail;
		$subject = 'Welcome to RiskGIS - please activate your account';
		$body = "Dear $mydisplayname,\n\nYou are receiving this email because you have registered with RiskGIS learning platform (http://riskgis.unil.ch:8080/riskgis/index.php). \n\nYour registered username is $myusername and you will need to activate your account before you can login. Please follow the link below to activate your account.\n\n http://riskgis.unil.ch:8080/riskgis/verify.php?ex=$id&code=$code  \n\nBest regards,\n\nThe RiskGIS Team";

		$headers = array(
			'From' => $from,
			'To' => $to,
			'Subject' => $subject
		);

		$smtp = Mail::factory('smtp', array(
				'host' => 'ssl://smtp.gmail.com',
				'port' => '465',
				'auth' => true,
				'username' => 'riskgis.igar.unil@gmail.com',
				'password' => 'riskgis-igar-zarchi'
			));

		$mail = $smtp->send($to, $headers, $body);

		if (PEAR::isError($mail)) {
			$_SESSION['message']= $mail->getMessage();			
		} else {
			$_SESSION['message']= 'We have sent an email to your registered address. Please click on the activation link in the email to create your account.';	
		}
	}	
	
	header('location:login.php?value='.$_SESSION['link']);
	exit();
	
	// https://daveismyname.com/login-and-registration-system-with-php-bp
	// http://www.codingcage.com/2015/09/login-registration-email-verification-forgot-password-php.html
	// https://github.com/1000hz/validator-remote-example
?>