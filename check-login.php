<?php
	require_once 'dbConnect.php'; // Connect to the database
	
	ob_start();
	session_start();
	
	if (!$dbconn) {
		echo '{"An error occurred.\n"}';
		exit;
	} 

	// username and password sent from form 
	$myusername=$_POST['form-username']; 
	$temp=$_POST['form-password']; 
	$mypassword = md5($temp);
	
	// assign database schema based on the exercise the user is entering for
	if ($_SESSION['link'] == 'one') {
		$workspace = 'riskgis'; 
	}
	elseif ($_SESSION['link'] == 'two') {
		$workspace = 'riskgis_two';
	}
	elseif ($_SESSION['link'] == 'three') {
		$workspace = 'riskgis_three'; 
	}
	
	// $query="SELECT * FROM public.users WHERE user_name='$myusername' and password='$mypassword';";	
	
	$query="SELECT * FROM $workspace.users WHERE user_name='$myusername';";
	$result = pg_query($dbconn,$query); // query the database
	$count = pg_num_rows($result); // pg_num_row is counting table row
	
	if($count==0){
		// if there is no such username, show error information that no account exists
		$_SESSION['error']= 'There is no account with this username! The username is sensititve, please make sure you enter it correctly and try again!';		
		header('location:login.php?value='.$_SESSION['link']);
		exit();
	}
	else{
		// if there is a username, check whether the account is activated/enabled
		$data = pg_fetch_object($result); //find the object of the selected record
		if ($data->flag_enabled == 0 || $data->flag_verified == 0){
			$_SESSION['error']= 'This account is either not activated or enabled! If you have not activated yet, please activate it through the activation link!';		
			header('location:login.php?value='.$_SESSION['link']);
			exit();
		}
		elseif ($data->flag_enabled == 1 && $data->flag_verified == 1 && $data->password === $mypassword){
			// Register $myusername, redirect to the application, otherwise stay at the login page.
			$_SESSION['usrname']= $myusername;
			$_SESSION['displayname']= $data->display_name;
			$_SESSION['email']= $data->email;
			$_SESSION['role']= $data->role;
			$_SESSION['userid']= $data->id;
			$_SESSION['password']= $_POST['form-password'];
			$_SESSION['workspace']= $workspace;
			
			// cURL session to set the cookies of geoserver
			$geoserverURL = "http://localhost:8080/geoserver/j_spring_security_check";
			$ch = curl_init($geoserverURL);	
			
			/* if ($_SESSION['role'] == 'admin') {
				$str = "username=admin&password=geoserver";
			} 
			else {
				$str = "username=public&password=public";
			} */
			
			$str = "username=admin&password=geoserver";
			
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_HEADER, 1);
			curl_setopt($ch, CURLOPT_POST, True);			
			curl_setopt($ch, CURLOPT_POSTFIELDS, $str);
			
			// grab URL and pass it to the browser
			$result = curl_exec($ch);	
			
			// extract cookie from the geoserver response 
			preg_match('/^Set-Cookie:\s*([^;]*)/mi', $result, $m);
			parse_str($m[1], $cookies);
			$cookieValue = $cookies['JSESSIONID'];

			$cookieName = "JSESSIONID";
			$cookieDomain = "http://localhost:8080";
			$cookiePath = "/geoserver";
			$cookieExpiration = 0;

			setcookie($cookieName,$cookieValue,$cookieExpiration,$cookiePath);

			// close cURL resource, and free up system resources
			curl_close($ch);
			
			// redirect to the specific application for different exercises, otherwise redirect to the main page
			if(isset($_SESSION['link'])){
				if ($_SESSION['link'] == 'one'){
					header("location:riskgis.php");				
				}
				elseif ($_SESSION['link'] == 'two'){
					header("location:../riskgis-two/riskgis.php");				
				}
				elseif ($_SESSION['link'] == 'three'){
					header("location:../riskgis-three/riskgis.php");
				}
				else header("location:index.php");	
			}
			else {
				header("location:index.php");	
			}		
			exit();
		}
		else {
			// if wrong login, redirect to the login page 
			$_SESSION['error']= 'Wrong password, please try again!';		
			header('location:login.php?value='.$_SESSION['link']);
			exit();
		}			
	}
	ob_end_flush();
?>