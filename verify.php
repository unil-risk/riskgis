<?php	
	require_once 'dbConnect.php'; // Connect to the database
	
	// if empty, redirect to index.php
	if(empty($_GET['ex']) || empty($_GET['code']))
	{
		header("location:index.php");	
	}
	
	// if not empty, retrieve values for activation
	if(isset($_GET['ex']) && isset($_GET['code']))
	{
		$id = base64_decode($_GET['ex']);
		$code = $_GET['code'];
		
		// check under which schema to query 
		if ($id == 1) $workspace = 'riskgis';
		elseif ($id == 2) $workspace = 'riskgis_two';
		elseif ($id == 3) $workspace = 'riskgis_three';	
		
		if (!$dbconn) {
			echo '{"An error occurred.\n"}';
			exit;
		}
		
		// query the table to check whether the user account is already activated
		$query= "SELECT * FROM ".$workspace.".users WHERE flag_verified = 1 AND code = '$code';";
		$result=pg_query($dbconn,$query);		
		$count=pg_num_rows($result);
		
		if($count>0){
			$msg = "Your account is already activated!"; 
		}
		else{
			// update the table 
			$query= "UPDATE ".$workspace.".users SET flag_verified = 1 WHERE code = '$code';";
			If (!$rs = pg_query($dbconn,$query)) {
				$msg = pg_last_error($dbconn);
			}
			else {
				$msg = "Your account is now activated!"; 
			}
		}					
	}
?>	

<!DOCTYPE html>

<html lang="en">
    <head>
		
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="">
		<meta name="author" content="">
      
        <title>Confirm Registration</title>
		
		<!-- CSS -->
        <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:400,100,300,500">
        <link rel="stylesheet" href="bootstrap-login-form/assets/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="bootstrap-login-form/assets/font-awesome/css/font-awesome.min.css">
		<link rel="stylesheet" href="bootstrap-login-form/assets/css/form-elements.css">
        <link rel="stylesheet" href="bootstrap-login-form/assets/css/style.css">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->

        <!-- Favicon and touch icons -->
        <link rel="shortcut icon" href="bootstrap-login-form/assets/ico/favicon.png">
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="bootstrap-login-form/assets/ico/apple-touch-icon-144-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="bootstrap-login-form/assets/ico/apple-touch-icon-114-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="bootstrap-login-form/assets/ico/apple-touch-icon-72-precomposed.png">
        <link rel="apple-touch-icon-precomposed" href="bootstrap-login-form/assets/ico/apple-touch-icon-57-precomposed.png">

    </head>
	
	<body>

        <!-- Top content -->
        <div class="top-content">
        	
            <div class="inner-bg">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-8 col-sm-offset-2 text">
                            <h1><strong>Welcome to RiskGIS</strong></h1>
							<h1>Confirmation of Registration </h1>
                            <div class="description">
								<p><?php if(isset($msg)) { echo $msg; } ?></p>
								<p><a href='index.php#signin'><strong>Login here</strong></a> for the exercise you registered.</p>
                            </div>
                        </div>
                    </div>
				</div>
            </div>
            
        </div>
		
		<!-- Footer -->
		<footer>
			<div class="container text-center">
				<p>Copyright &copy; <a href="http://wp.unil.ch/risk/">Risk Analysis</a> group 2016</p>
			</div>
		</footer>
		
		<!-- Javascript -->		
        <script src="bootstrap-login-form/assets/js/jquery-1.11.1.min.js"></script>
        <script src="bootstrap-login-form/assets/bootstrap/js/bootstrap.min.js"></script>
        <script src="bootstrap-login-form/assets/js/jquery.backstretch.min.js"></script>
        <script src="bootstrap-login-form/assets/js/scripts.js"></script>
        
        <!--[if lt IE 10]>
            <script src="assets/js/placeholder.js"></script>
        <![endif]-->

    </body>

</html>