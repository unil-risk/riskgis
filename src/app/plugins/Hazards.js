/**
 * Copyright (c) 2016 
 * 
 * Published under the GPL license.
 */

/**
 * @requires plugins/Tool.js
 * @requires Ext.ux.util.js
 */

/** api: (define)
 *  module = riskgis.plugins
 *  class = Hazards
 */

/** api: (extends)
 *  plugins/Tool.js
 */
Ext.namespace("riskgis.plugins");

/** api: constructor
 *  .. class:: Hazards(config)
 *
 *    Provides action for creating and viewing the list of available hazard layers.
 */
riskgis.plugins.Hazards = Ext.extend(gxp.plugins.Tool, {
    
    /** api: ptype = riskgis_Hazards */
    ptype: "riskgis_Hazards", 
	
	/** api: config[hazardMenuText]
     *  ``String``
     *  Text for the menu item (i18n).
     */
    hazardMenuText: "Hazard",

    /** api: config[hazardActionTip]
     *  ``String``
     *  Text for the action tooltip (i18n).
     */
    hazardActionTip: "Tool to view the list of hazard layers",
	
	addActions: function() {
		var actions = riskgis.plugins.Hazards.superclass.addActions.apply(this, [			 						
		{
			menuText: this.hazardMenuText,
            iconCls: "riskgis-icon-hazard",
            tooltip: this.hazardActionTip,
			scope: this,
            handler: function(args) {
				if (args == 'view') this.listLayers();				
			}			
		}
		]);
	},
	
	/** api: method[listLayers]
     */
	listLayers: function() {
		var hazStore = new Ext.data.GroupingStore({
				url: 'hazInfo.php',
				baseParams: {
					task: 'load',
					ws: 'riskgis'
				},
				sortInfo:{field: 'nom', direction: "ASC"},
				groupField:'type',
				autoLoad: true,
				reader: new Ext.data.JsonReader({
					totalProperty : 'totalCount',
					root          : 'rows',
					successProperty: 'success',
					idProperty    : 'id',
					fields: [
						{name : 'id', type : 'int'},
						{name : 'nom', type : 'String'},
						{name : 'description', type : 'String'},
						{name : 'type', type : 'String'},						
						{name : 'ans', type : 'int'},
						{name : 'remarques', type : 'String'},
						{name : 'nom_origine', type : 'String'},
						{name : 'indice', type : 'String'}						
					] 
				})
		});	
		
		var expander = new Ext.grid.RowExpander({
			tpl : new Ext.Template(
				'<p><b>Description:</b> {description}</p>',
				'<p><b>Remarks:</b> {remarques}</p>'
			)
		});
		
		var hazGrid = new Ext.grid.GridPanel({
			store: hazStore,
			colModel: new Ext.grid.ColumnModel({
				columns: [			
					expander,
					{header: "ID", dataIndex: 'id', hidden: true},
					{header: "Type", dataIndex: 'type'},					
					{header: "Layer Name", dataIndex: 'nom'},
					{header: "Description", dataIndex: 'description', hidden: true},
					{header: "Return Period", dataIndex: 'ans'},
					{header: "Remarks", dataIndex: 'remarques', hidden: true},			
					{header: "Mapping Index", dataIndex: 'indice', hidden: true},
					{header: "Original Name", dataIndex: 'nom_origine', hidden: true},
					{
						xtype: 'actioncolumn',
						items: [
						{
							icon: 'src/gxp/theme/img/silk/map.png',  
							tooltip: 'Visualize the hazard map',								
							handler: function(grid, rowIndex, colIndex) {
								// to load the layer to the map 
								var rec = grid.getStore().getAt(rowIndex);
								var lyr_name = rec.get('indice');
								var names = {};
								names[lyr_name] = true;
										
								var source = app.tools.addlyrs.target.layerSources["local"]; // need to reassign the source in case other sources such as google are being added by users
								app.tools.addlyrs.setSelectedSource(source);
								app.tools.addlyrs.selectedSource.store.load({
									callback: function(records, options, success) {
										var gridPanel, sel;
										if (app.tools.addlyrs.capGrid && app.tools.addlyrs.capGrid.isVisible()) {
											gridPanel = app.tools.addlyrs.capGrid.get(0).get(0);
											sel = gridPanel.getSelectionModel();
											sel.clearSelections();
										}
										// select newly added layers
										var newRecords = [];
										var last = 0;
										app.tools.addlyrs.selectedSource.store.each(function(record, index) {
											if (record.get("name") in names) {
												last = index;
												newRecords.push(record);
											}
										});
										if (gridPanel) {
											// this needs to be deferred because the 
											// grid view has not refreshed yet
											window.setTimeout(function() {
												sel.selectRecords(newRecords);
												gridPanel.getView().focusRow(last);
											}, 100);
										} else {
											app.tools.addlyrs.addLayers(newRecords, true);
										}                                                                                    
									},
									scope: this                                            
								});								
							}								
						}]
					}	
				],
				defaults: {
					sortable: true,
					menuDisabled: true,
					width: 5
				}
			}),	
			view: new Ext.grid.GroupingView({				
				forceFit: true,
				groupTextTpl: '{text} ({[values.rs.length]} {[values.rs.length > 1 ? "Items" : "Item"]})'
			}),						
			frame:true,
			width: 700,
			height: 450,
			plugins:[expander,
				new Ext.ux.grid.Search({
					iconCls:'gxp-icon-zoom-to',
					disableIndexes:['id','indice','nom_origine'],
					minChars:2,
					autoFocus:true,
					width: 150,
					mode: 'local'
				})],
			bbar: [{
				xtype: 'tbtext',
				id: 'listHazTotalRec',
				text: 'Loading ...'
			},'->']
		});
		
		hazGrid.getStore().on('load', 					
			function (store, records, options) {
				Ext.getCmp('listHazTotalRec').setText('Total no. of hazard layers: ' + records.length.toString());
			}
		);
				
		var hazInfoWin = new Ext.Window({
			title: 'Hazard layers information',
			layout: 'fit',
			plain:false,
			maximizable: true,
			collapsible: true,
			closeable: true,
			buttonAlign:'right',
			items: hazGrid,
			tools:[
					{
						id:'help',
						qtip: 'Get Help',
						handler: function(event, toolEl, panel){
							Ext.Msg.show({
								title:'Help Information',
								msg: '<b>' + 'Intensity map' + '</b>' + ' indicates intensities of dangerous processes for different standard frequencies, and do not consider the vulnerability of the objects present on the territory.' + '<br><br>' + 'The intensity maps are used for risk analysis, since they assess the frequency and intensity of an event. To learn more about the difference between magnitude and intensity of earthquakes, read ' + '<a href="https://earthquake.usgs.gov/learn/topics/mag_vs_int.php" target="_blank">here</a>.',
								buttons: Ext.Msg.OK,															   
								animEl: 'elId',
								icon: Ext.MessageBox.INFO
							});		
					}
				}, {
						id:'search',
						qtip: 'Play Video',
						handler: function(event, toolEl, panel){
							vid = new Ext.Window({
								title: 'Video Tutorial: visualization of hazard layers',
								resizable: false,
								html: '<iframe width="560" height="315" src="https://www.youtube.com/embed/fIztz1OMrAM?rel=0&amp;start=70" frameborder="0" allowfullscreen></iframe>'
							});
							
							vid.show();
					}
				}]
		});
		
		hazInfoWin.show();
	}	

});	

Ext.preg(riskgis.plugins.Hazards.prototype.ptype, riskgis.plugins.Hazards);