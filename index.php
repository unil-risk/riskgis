<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>RiskGIS - A learning environment for risk management of natural hazards</title>
	<link rel="shortcut icon" href="./favicon.ico">
	 
    <!-- Bootstrap Core CSS -->
    <link href="startbootstrap-grayscale-1.0.6/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="startbootstrap-grayscale-1.0.6/css/grayscale.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="startbootstrap-grayscale-1.0.6/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="http://fonts.googleapis.com/css?family=Lora:400,700,400italic,700italic" rel="stylesheet" type="text/css">
    <link href="http://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body id="page-top" data-spy="scroll" data-target=".navbar-fixed-top">

    <!-- Navigation -->
    <nav class="navbar navbar-custom navbar-fixed-top" role="navigation">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-main-collapse">
                    <i class="fa fa-bars"></i>
                </button>
                <a class="navbar-brand page-scroll" href="#page-top">
                    <i class="fa fa-play-circle"></i>  <span class="light">RiskGIS</span> 
                </a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse navbar-right navbar-main-collapse">
                <ul class="nav navbar-nav">
                    <!-- Hidden li included to remove active class from about link when scrolled up past about section -->
                    <li class="hidden">
                        <a href="#page-top"></a>
                    </li>
					<li>
                        <a class="page-scroll" href="#signin">Getting Started</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="#about">About</a>
                    </li>                   
                    <li>
                        <a class="page-scroll" href="#contact">Contact</a>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>

    <!-- Intro Header -->
    <header class="intro">
        <div class="intro-body">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 col-md-offset-2">
                        <h1 class="brand-heading">RiskGIS</h1>
                        <p class="intro-text">A free and open-source webGIS based learning environment for environmental risk.<br>Developed by Risk Analysis group.</p>
                        <a href="#signin" class="btn btn-circle page-scroll">
                            <i class="fa fa-angle-double-down animated"></i>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </header>

	<!-- About Sign in -->
    <section id="signin" class="container content-section text-center">        
		<div class="row">
			<div class="col-lg-8 col-lg-offset-2">
				<h2>Getting Started</h2>
				<p>Sign in to the RiskGIS application to start learning how an environmental risk system functions.</p>		
				<ul class="list-inline banner-social-buttons">                   
					<li><a href="login.php?value=one" class="btn btn-default btn-lg" name="one" id="exeOne">Exercise I</a></li> 
					<li><a href="login.php?value=two" class="btn btn-default btn-lg" name="two" id="exeTwo">Exercise II</a></li>
					<li><a href="login.php?value=three" class="btn btn-default btn-lg" name="three" id="exeThree">Exercise III</a></li>
				</ul>
			</div>
		</div>		
    </section>

    <!-- About Section -->
    <section id="about" class="content-section text-center"> 
		<div class="download-section">
			<div class="container">
				<div class="row">
					<div class="col-lg-8 col-lg-offset-2">
						<h2>About RiskGIS</h2>
						<p>RiskGIS is an interactive web-GIS based application for students in learning and understanding of how environment risk systems function.</p> 
						<p>It is developed based on open-source software solutions, backed up by <a href="http://boundlessgeo.com/">Boundless</a> framework with its client-side <a href="http://developers.boundlessgeo.com/">SDK</a> development environment.</p>
						<p>This project is funded by <a href="http://www.unil.ch/fip/fr/home.html">Innovative Teaching Fund (FIP)</a> of the University of Lausanne.</p>
					<!-- <a href="" class="btn btn-default btn-lg">Visit Documentation Page</a> -->
					</div>
				</div>	
			</div>
		</div>	
    </section>

    <!-- Contact Section -->
    <section id="contact" class="container content-section text-center">
				<div class="row">
					<div class="col-lg-8 col-lg-offset-2">
						<h2>Contact RiskGIS team</h2>
						<p>Feel free to contact us to provide some feedback on this learning application, give us suggestions for improvements, or for more information!</p>
						<p><a href="mailto:riskgis.igar.unil@gmail.com">riskgis.igar.unil@gmail.com</a>
						</p>
						<ul class="list-inline banner-social-buttons">                   
							<li>
								<a href="https://github.com/" class="btn btn-default btn-lg"><i class="fa fa-github fa-fw"></i> <span class="network-name">Github</span></a>
							</li>
							<li>
								<a href="https://plus.google.com/u/0/114426804459266373926" class="btn btn-default btn-lg"><i class="fa fa-google-plus fa-fw"></i> <span class="network-name">Google+</span></a>
							</li>
						</ul>
					</div>
				</div>			
    </section>   

    <!-- Footer -->
    <footer>
        <div class="container text-center">
            <p>Copyright &copy; <a href="http://wp.unil.ch/risk/">Risk Analysis</a> group 2016</p>
        </div>
    </footer>

    <!-- jQuery -->
    <script src="startbootstrap-grayscale-1.0.6/js/jquery.js"></script>
	
	<!-- Script to disable the buttons -->	
	<script>	
		$('#exeOne').addClass('disabled');
		$('#exeOne').removeAttr('data-toggle'); 	
		$('#exeTwo').addClass('disabled');
		$('#exeTwo').removeAttr('data-toggle');
	//	$('#exeThree').addClass('disabled');
	//	$('#exeThree').removeAttr('data-toggle');
	</script>
	
	
    <!-- Bootstrap Core JavaScript -->
    <script src="startbootstrap-grayscale-1.0.6/js/bootstrap.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="startbootstrap-grayscale-1.0.6/js/jquery.easing.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="startbootstrap-grayscale-1.0.6/js/grayscale.js"></script>

</body>

</html>