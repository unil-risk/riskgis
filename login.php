<!DOCTYPE html>
<?php
	session_start();
	if((isset($_GET['value'])) || (isset($_SESSION['link']))){
		$_SESSION['link'] = $_GET['value'];
	}
	else {
		header("location:index.php");	
	}
?>

<html lang="en">
    <head>
		
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="">
		<meta name="author" content="">
      
        <title>Login Form</title>

        <!-- CSS -->
        <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:400,100,300,500">
        <link rel="stylesheet" href="bootstrap-login-form/assets/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="bootstrap-login-form/assets/font-awesome/css/font-awesome.min.css">
		<link rel="stylesheet" href="bootstrap-login-form/assets/css/form-elements.css">
        <link rel="stylesheet" href="bootstrap-login-form/assets/css/style.css">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->

        <!-- Favicon and touch icons -->
        <link rel="shortcut icon" href="bootstrap-login-form/assets/ico/favicon.png">
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="bootstrap-login-form/assets/ico/apple-touch-icon-144-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="bootstrap-login-form/assets/ico/apple-touch-icon-114-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="bootstrap-login-form/assets/ico/apple-touch-icon-72-precomposed.png">
        <link rel="apple-touch-icon-precomposed" href="bootstrap-login-form/assets/ico/apple-touch-icon-57-precomposed.png">

    </head>

    <body>

        <!-- Top content -->
        <div class="top-content">
        	
            <div class="inner-bg">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-8 col-sm-offset-2 text">
                            <h1><strong>RiskGIS</strong> Login Form</h1>
                            <div class="description">
                            	<p>
	                            	Login to the application to start learning!
                            	</p>
                            </div>
                        </div>
                    </div>
					<?php
						session_start();
						if(isset($_SESSION['message'])) {
							echo '<div><div class="alert alert-info alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>'.$_SESSION['message'].'</div></div>';
							unset($_SESSION['message']);
						}		
					?>
                    <div class="row">
                        <div class="col-sm-5">
							<div class="form-box">
								<div class="form-top">
									<div class="form-top-left">
										<h3>Login to RiskGIS</h3>
										<p>Enter your username and password to log on:</p>
									</div>
									<div class="form-top-right">
										<i class="fa fa-key"></i>
									</div>
								</div>
								<div class="form-bottom">
									<form role="form" action="check-login.php" method="post" class="login-form">
										<div class="form-group">
											<label class="sr-only" for="form-username">Username</label>
											<input type="text" name="form-username" placeholder="Username..." class="form-username form-control" id="form-username">
										</div>
										<div class="form-group">
											<label class="sr-only" for="form-password">Password</label>
											<input type="password" name="form-password" placeholder="Password..." class="form-password form-control" id="form-password">
										</div>
										<button type="submit" class="btn">Sign in!</button>
									</form>
								</div>
							</div>	
							<?php
								session_start();
								if(isset($_SESSION['error'])) { 
									echo '<div><div class="alert alert-danger"><strong>Attention!</strong> '.$_SESSION['error'].'</div></div>';
									unset($_SESSION['error']);
								}		
							?>
							
							<div class="description">
                            	<p>
									<?php
										echo '<a href="forgot-pass.php?value='.$_GET['value'].'">Forgot your password?</a>';
									?>
								</p>								
								<p>
									<?php
										echo '<a href="resend-activation.php?value='.$_GET['value'].'">Resend activation email</a>';
									?>
								</p>
                            </div>
							
						<!--<div class="social-login">
	                        	<h3>...or login with:</h3>
	                        	<div class="social-login-buttons">
		                        	<a class="btn btn-link-1 btn-link-1-facebook" href="#">
		                        		<i class="fa fa-facebook"></i> Facebook
		                        	</a>
		                        	<a class="btn btn-link-1 btn-link-1-twitter" href="#">
		                        		<i class="fa fa-twitter"></i> Twitter
		                        	</a>
		                        	<a class="btn btn-link-1 btn-link-1-google-plus" href="#">
		                        		<i class="fa fa-google-plus"></i> Google Plus
		                        	</a>
	                        	</div>
	                        </div>-->
							
                        </div>
						
						<div class="col-sm-1 middle-border"></div>
                        <div class="col-sm-1"></div>
						
						<div class="col-sm-5">
                        	
                        	<div class="form-box">
                        		<div class="form-top">
	                        		<div class="form-top-left">
	                        			<h3>Let's get started!</h3>
	                            		<p>Fill in the form below to register:</p>
	                        		</div>
	                        		<div class="form-top-right">
	                        			<i class="fa fa-pencil"></i>
	                        		</div>
	                            </div>
	                            <div class="form-bottom">
				                    <form data-toggle="validator" role="form" action="sign-up.php" method="post" class="registration-form">
				                    	<div class="form-group">
				                    		<label class="sr-only" for="form-user-name">User name</label>
				                        	<input type="text" name="form-user-name" placeholder="User name..." class="form-user-name form-control" id="form-user-name" required>
				                        </div>
				                        <div class="form-group">
				                        	<label class="sr-only" for="form-display-name">Display name</label>
				                        	<input type="text" name="form-display-name" placeholder="Display name..." class="form-display-name form-control" id="form-display-name" required>											
				                        </div>
				                        <div class="form-group">
				                        	<label class="sr-only" for="form-email">Email</label>
				                        	<input type="email" name="form-email" placeholder="Email..." class="form-email form-control" id="form-email" data-error="Bruh, that email address is invalid!" required>
											<div class="help-block with-errors"></div>
									   </div>
										<div class="form-group">
											<label class="sr-only" for="form-pass-word">Password</label>
											<input type="password" data-minlength="6" name="form-pass-word" placeholder="Password (minimum of 6 characters)..." class="form-password form-control" id="form-pass-word" required>
										</div>
										<div class="form-group">
											<label class="sr-only" for="form-confirm-pass-word">Confirm Password</label>
											<input type="password" name="form-confirm-pass-word" data-match="#form-pass-word" data-match-error="Whoops, these don't match!" placeholder="Re-enter your password..." class="form-password form-control" id="form-confirm-pass-word" required>
											<div class="help-block with-errors"></div>
										</div>				                        
				                        <button type="submit" class="btn" id="signButton">Sign me up!</button>
				                    </form>
			                    </div>
                        	</div>                      
							
                        </div>
                    </div>                   
                </div>
            </div>
            
        </div>

		<!-- Footer -->
		<footer>
			<div class="container text-center">
				<p>Copyright &copy; <a href="http://wp.unil.ch/risk/">Risk Analysis</a> group 2016</p>
			</div>
		</footer>
		
        <!-- Javascript -->		
        <script src="bootstrap-login-form/assets/js/jquery-1.11.1.min.js"></script>
        <script src="bootstrap-login-form/assets/bootstrap/js/bootstrap.min.js"></script>
        <script src="bootstrap-login-form/assets/js/jquery.backstretch.min.js"></script>
        <script src="bootstrap-login-form/assets/js/scripts.js"></script>
		<script src="bootstrap-login-form/assets/js/validator.js"></script>		
        <!-- <script>
			document.getElementById("signButton").disabled = true;;
		</script> -->	
        <!--[if lt IE 10]>
            <script src="assets/js/placeholder.js"></script>
        <![endif]-->

    </body>

</html>