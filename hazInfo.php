<?php
	require_once 'dbConnect.php'; // Connect to the database

	$workspace = $_POST['ws'];
	$task = $_POST['task'];	
		
	if (!$dbconn){
		echo "An error occured.\n";
		exit;
	}
	
	if ($task == 'loadSelected') {
		$hazType = $_POST['type'];
		
		$query = "SELECT nom, indice FROM ".$workspace.".hazards WHERE type = '$hazType';";				 
		$arr=array();
		
		If (!$rs = pg_query($dbconn,$query)) {
			Echo '{success:false,message:'.json_encode(pg_last_error($dbconn)).'}';
		}
		else {
				while($obj = pg_fetch_object($rs)){
				$arr[] = $obj;
			}
				
			Echo '{success:true,rows:'.json_encode($arr).'}';
		}
	}
	
	if ($task == 'load') {
		
		$query = "SELECT * FROM ".$workspace.".hazards;";
		$arr=array();
		
		If (!$rs = pg_query($dbconn,$query)) {
			Echo '{success:false,message:'.json_encode(pg_last_error($dbconn)).'}';
		}
		else {
				while($obj = pg_fetch_object($rs)){
				$arr[] = $obj;
			}
				
			Echo '{success:true,rows:'.json_encode($arr).'}';
		}
	}
?>