<?php
	require_once "Mail.php"; // Pear Mail Library	
	require_once 'dbConnect.php'; // Connect to the database
	
	session_start();

	if(isset($_POST['btn-submit'])){
		// get the posted data
		$myusername=$_POST['form-username']; 
		$myemail=$_POST['form-email'];

		// assign database schema based on the exercise the user is entering for
		if ($_GET['value'] == 'one') {
			$workspace = 'riskgis'; 
			$id = base64_encode(1);
		}
		elseif ($_GET['value'] == 'two') {
			$workspace = 'riskgis_two';
			$id = base64_encode(2);
		}
		elseif ($_GET['value'] == 'three') {
			$workspace = 'riskgis_three'; 
			$id = base64_encode(3);
		}		
		
		if (!$dbconn) {
			echo '{"An error occurred.\n"}';
			exit;
		}
		
		// query to check if this username and email address was registered
		$query="SELECT * FROM $workspace.users WHERE user_name = '$myusername' AND email = '$myemail';";
		$result = pg_query($dbconn,$query); // query the database
		$count = pg_num_rows($result); // pg_num_row is counting table row
		
		if ($count == 1){
			$data = pg_fetch_object($result); //find the object of the selected record
			$mydisplayname = $data->display_name;			
			$code = $data->code; 
			
			// data to send activation link
			$from = 'riskgis.igar.unil@gmail.com';
			$to = $myemail;
			$subject = 'Welcome to RiskGIS - your activation link';
			$body = "Dear $mydisplayname,\n\nYou are receiving this email because you have registered with RiskGIS learning platform (http://riskgis.unil.ch:8080/riskgis/index.php). \n\nYour registered username is $myusername and you will need to activate your account before you can login. Please follow the link below to activate your account.\n\n http://riskgis.unil.ch:8080/riskgis/verify.php?ex=$id&code=$code  \n\nBest regards,\n\nThe RiskGIS Team";

			$headers = array(
				'From' => $from,
				'To' => $to,
				'Subject' => $subject
			);

			$smtp = Mail::factory('smtp', array(
					'host' => 'ssl://smtp.gmail.com',
					'port' => '465',
					'auth' => true,
					'username' => 'riskgis.igar.unil@gmail.com',
					'password' => 'riskgis-igar-zarchi'
				));

			$mail = $smtp->send($to, $headers, $body);

			if (PEAR::isError($mail)) {
				$msg = $mail->getMessage();			
			} else {
				$msg = 'We have sent an email to '.$myemail.'. Please click on the activation link in the email to activate your account.';	
			}
		}
		else{
			$error = 'Wrong username or email address. Please make sure you entered data correctly and try again!';	
		}
	
	}
	
?>
	
<!DOCTYPE html>

<html lang="en">
    <head>
		
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="">
		<meta name="author" content="">
      
        <title>Resend Activation</title>
		
		<!-- CSS -->
        <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:400,100,300,500">
        <link rel="stylesheet" href="bootstrap-login-form/assets/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="bootstrap-login-form/assets/font-awesome/css/font-awesome.min.css">
		<link rel="stylesheet" href="bootstrap-login-form/assets/css/form-elements.css">
        <link rel="stylesheet" href="bootstrap-login-form/assets/css/style.css">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->

        <!-- Favicon and touch icons -->
        <link rel="shortcut icon" href="bootstrap-login-form/assets/ico/favicon.png">
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="bootstrap-login-form/assets/ico/apple-touch-icon-144-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="bootstrap-login-form/assets/ico/apple-touch-icon-114-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="bootstrap-login-form/assets/ico/apple-touch-icon-72-precomposed.png">
        <link rel="apple-touch-icon-precomposed" href="bootstrap-login-form/assets/ico/apple-touch-icon-57-precomposed.png">

    </head>
	
	<!-- Top content -->
        <div class="top-content">
        	
            <div class="inner-bg">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-8 col-sm-offset-2 text">
                            <h1><strong>RiskGIS</strong> Account Activation</h1>
                            <div class="description">
								<p>Please enter your username and the email address you used to sign up, and your activation email will be resent.</p>
								<p>
									<?php 
										if(isset($msg)) {
											echo '<div><div class="alert alert-info alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>'.$msg.'</div></div>'; 
										}
									?>
								</p>
							</div>							
                        </div>
                    </div>
					<div class="row">
                        <div class="col-sm-6 col-sm-offset-3 form-box">
							<div class="form-box">
								<div class="form-top">
									<div class="form-top-left">
										<h3>Account Activation</h3>
										<p>Enter data to resend activation:</p>
									</div>
									<div class="form-top-right">
										<i class="fa fa-pencil"></i>
									</div>
								</div>
								<div class="form-bottom">
									<form data-toggle="validator" role="form" action="" method="post" class="resend-form">
										<div class="form-group">
											<label class="sr-only" for="form-username">Username</label>
											<input type="text" name="form-username" placeholder="Username..." class="form-username form-control" id="form-username" required>
										</div>
										<div class="form-group">
											<label class="sr-only" for="form-email">Email</label>
				                        	<input type="email" name="form-email" placeholder="The email address you used to register..." class="form-email form-control" id="form-email" data-error="Bruh, that email address is invalid!" required>
											<div class="help-block with-errors"></div>
										</div>
										<button type="submit" name="btn-submit" class="btn">Resend Activation Email!</button>
									</form>
								</div>
							</div>
							<?php
								if(isset($error)) { 
									echo '<div><div class="alert alert-danger"><strong>Attention!</strong> '.$error.'</div></div>';
								}		
							?>
						</div>	
					</div>	
				</div>
            </div>
            
        </div>
		
		<!-- Footer -->
		<footer>
			<div class="container text-center">
				<p>Copyright &copy; <a href="http://wp.unil.ch/risk/">Risk Analysis</a> group 2016</p>
			</div>
		</footer>
		
		<!-- Javascript -->		
        <script src="bootstrap-login-form/assets/js/jquery-1.11.1.min.js"></script>
        <script src="bootstrap-login-form/assets/bootstrap/js/bootstrap.min.js"></script>
        <script src="bootstrap-login-form/assets/js/jquery.backstretch.min.js"></script>
        <script src="bootstrap-login-form/assets/js/scripts.js"></script>
		<script src="bootstrap-login-form/assets/js/validator.js"></script>		
        
        <!--[if lt IE 10]>
            <script src="assets/js/placeholder.js"></script>
        <![endif]-->

    </body>

</html>